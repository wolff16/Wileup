#!/usr/bin/perl
use strict;
use warnings;
use 5.010;
use Getopt::Long qw(GetOptions);
use Term::ANSIColor;
Getopt::Long::Configure qw(gnu_getopt);
use Data::Dumper qw(Dumper);
use Text::ParseWords;
use File::Basename;
use List::Util qw(max);

# use cpan and then type 'install Statistics::R'
# use Statistics::R ;
use autodie qw(open close);


use constant FALSE => 1==0;
use constant TRUE => not FALSE;
my $version_number= "1.0";
my $scriptdir = dirname(__FILE__);

# add your samtools path here if its not globaly installed

#my $samtools="samtools";

my $samtools="/SAN/bioinf_core/programs/samtools-1.5/samtools-1.5/samtools";

## 1.0
## [FIX] Fixed a bug, where the output file was empty
## when there was no positon from the panel pileuped from the iput bam file. 
## This could occure when the bam file was subsampled from an original bam file and not all position where present.
## With this last fix the script will be in release state.
## 0.9.1
## added a gsubing of chromosome contigs to always preserve always matching hash_id , example: chr1 -> 1
## added a threshold in the case the max_n = alt allel but only with low reads (1,2,3) etc. -> no real readsupports here
## 0.9
## alpha tested on RNA-seq data set
## 0.8
## implementation of panel calling, specific mutation calling for a preselected amount of positions 
## 0.7
## implementation of civic annotation/ interpretation of mutations
## 0.6
## implementation of cosmic parsing and annotation
## 0.5
## implementation of clinvar parsing and annotation

my $ref= "";
my $aligned = "";
my $verbose = ""; #default is false
my $clinvar ="";
my $cosmic =""; # use cosmic2table.pl to prepare it
my $civic ="";
my $header = "";
my @varfound = ();
my @saved_entries_clinvar ;
my @saved_entries_cosmic = ();
my @saved_entries_civic = ();
my @db_select = ();
my $custom_tracks="";
# for panel mode
my $panelmode= "",
my $panel= "",

my $help = '';
############################
## Criteria for variants  ##
############################

## reverse hash for the nucleotide position on negative strands in cosmics
## CDS sequences

my %reverseNucleotides = (
      "A" => "T",
      "T" => "A",
      "C" => "G",
      "G" => "C",
      "a" => "t",
      "t" => "a",
      "c" => "g",
      "g" => "c"
  );

my $treshhold=3; # minimal amount of count to be counted as variant
my $perc_treshhold=0.10; # minimal percentage to be counted as variant
my $skip_variants =0; # default we will not skip variants we have not find any annotations


sub print_options() {
##### displays title
  print STDERR "\n\n";
    print STDERR "\t################################################################################\n";
    print STDERR "\t###                                                                          ###\n";
    print STDERR "\t###                    wileup.pl                                             ###\n";
    print STDERR "\t###       A tool for variant detection & annotation                          ###\n";
    print STDERR "\t###                 utilizing RNA-Seq data.                                  ###\n";
    print STDERR "\t###                   ver. $version_number                          	             ###\n";
    print STDERR "\t###                                                                          ###\n";
    print STDERR "\t###  options:                                                                ###\n";
    print STDERR "\t###   --reference|-r <reference.fa>                                          ###\n";
    print STDERR "\t###   --alignmentfile|-a <aln.sorted.bam>                                    ###\n";
    print STDERR "\t###   --perc_tresh|-p <min. alternative allele fraction>    [0.10 default]   ###\n";
    print STDERR "\t###   --tresh|-t      <min. count of alternative allele>    [   3 default]   ###\n";
    print STDERR "\t###   --verbose|-v                                                           ###\n";
    print STDERR "\t###   --help|-h                                                              ###\n"; 
    print STDERR "\t###                                                                          ###\n";
    print STDERR "\t###   specific option for a panel run                                        ###\n";
    print STDERR "\t###    --panelmode|m                                                         ###\n";
    print STDERR "\t###    --panel|-p <CGI_CIVIC_SNVs.csv>                                       ###\n";
    print STDERR "\t###                                                                          ###\n";
    print STDERR "\t###                                                                          ###\n";
    print STDERR "\t###   specific option for a complete run                                     ###\n";
    print STDERR "\t###    --clinvar|-c <clinvarDB.summary.tsv>                                  ###\n";
    print STDERR "\t###    --cosmic|-o <cosmic.processed.tsv>                                    ###\n";
    print STDERR "\t###    --civic|-i <CGI_CIVIC_SNVs.csv>                                       ###\n";
    print STDERR "\t###    --custom_tracks|-t <mpileup.list>                                     ###\n";
    print STDERR "\t###    --skip_variants|-s                                                    ###\n";
    print STDERR "\t###                                                                          ###\n";
    print STDERR "\t###   Alexander Wolff, 2018                                                  ###\n";
    print STDERR "\t###                                                                          ###\n";
    print STDERR "\t###    For questions and help:                                               ###\n";
    print STDERR "\t###    alexander.wolff\@med.uni-goettingen.de                                 ###\n";
    print STDERR "\t###                                                                          ###\n";
    print STDERR "\t################################################################################\n\n\n";
    exit 1;
}


## parse Command line operations
GetOptions(
    'reference|r=s' => \$ref,
    'alignmentfile|a=s' => \$aligned,
    'tresh|t=i' =>\$treshhold,
    'perc_tresh|t=f' =>\$perc_treshhold,
    'clinvar|c=s' => \$clinvar,
    'cosmic|o=s' => \$cosmic,
    'civic|i=s' => \$civic,
    'panel|p=s' => \$panel,
    'verbose|v' => \$verbose,
    'panelmode|m' => \$panelmode,
    'custom_tracks|t=s' => \$custom_tracks,
    'help|h' => \$help,
    'skip_variants|s' => \$skip_variants
) or die print_options();



##### if help flag is active:
if ($help ){
    print_options();
    exit 1;
}
## -I no Indel Calling performed
## -f faidx reference file
## filtering arround indels with BAQ -E option ?=
if ($panelmode){
    
    # read in panel file which includes mutation positions and annotation, from civic/cgi 
    # it got specific clinical implications for a preselected mutation of 445 mutations by now
    
    my $input_panel = $panel;
    my %genes = ();
    my %p_variant = ();
    my %c_variant = ();
    my %chrom = ();
    my %chromStart = ();
    my %chromEnd = ();
    my %ref_allel = () ;
    my %alt_allel = ();
    my %source = ();
    my %drug = ();
    my $hash_id="";
    #############
    
    my @line = ();
    
    print STDERR "[INFO] ",scalar(localtime),": Reading and pileuping $aligned.\n" if $verbose;
    

    open(my $fp, " $samtools mpileup -I -B -a -l $scriptdir/Annotation/20180521_panel.list -f $ref $aligned |") or die "Could not open file, $!";;
    # -B disable BAQ computation
    # -I do not perform indel calling
    ## example output
    # $chr     $pos   $ref    $n       $b
    ## 1       10541   C       1       ^",     D
    ## 1       10542   C       2       ,^",    DA

    ## Annotation to the matching sheme in $b

    ##  At the read base column,
    ## .  a dot stands for a match to the reference base on the forward strand,
    ## ,  a comma for a match on the reverse strand,
    ## `ACGTN' for a mismatch on the forward strand and
    ## `acgtn' for a mismatch on the reverse strand. A pattern
    ## `\+[0-9]+[ACGTNacgtn]+' indicates there is an insertion between this reference position and the next reference position. The length of the insertion is given by the integer in the
    ##             pattern, followed by the inserted sequence. Here is an example of 2bp insertions on three reads: seq2 156 A 11  .$......+2AG.+2AG.+2AGGG    <975;:<<<<<
    ## -[0-9]+[ACGTNacgtn]+'   represents a deletion from the reference. Here is an example of a 4bp deletions from the reference, supported by two reads:
    ##             seq3 200 A 20 ,,,,,..,.-4CACC.-4CACC....,.,,.^~. ==<<<<<<<<<<<::<;2<<
    ## `^'             marks the start of a read segment which is a contiguous subsequence on the read separated by `N/S/H' CIGAR operations
    ## `$'             marks the end of a read segment.
    ## '>' or '<' for a reference skip

	#save pileup data
    my %chr_h=();
    my %pos_h=();
    my %ref_h=();
    my %a_h=();
    my %c_h=();
    my %g_h=();
    my %t_h=();
    my %n_h=();
    my %b_h=();
    my %q_h=();
    my %evidence_h=();
    my %rn_h=();
    my %maxb_h=();
    my %max_h=();

    while(<$fp>) {

        ## ^    Start of line
        ## \w   Any word character (letter, number, underscore)
        ## \d   Any digit
        ## \S   Any non-whitespace character
        ## \s   Any whitespace character
        if(/^(\w+)\t(\d+)\t(\w+)\t(\d+)\t(\S+)\t(\S+)\s*$/) {
            #print STDERR "$_\n";
            ## saving meta information of the line
            ## chromosome name
           	# first check if the hash_id is there 
            my $chr=$1;
            # get rid of any inconvinience with chr1 - 1, since they will not match later on wenn using hash keys
            # function is written in the last section
        
            ## reference nucleotide position /1-based coordinate
            my $pos=$2;

            ## reference nucleotide 
            my $ref=$3;

            ## readdepth at the position
   	        # seems also to count stuff which is not reference or alternativ allel from time to time, 
   	        # therefore i have to count it myself otherwise a,c,g,t would not sum up to n
   	        # ill overwrite it later on
   	        my $n=$4;
            #read bases
            my $b=$5;

            ## base qualities [not utilized yet]
            my $q=$6;

            #$chr =~ s/chr//i; reverted for now
            $hash_id ="$chr:$pos:$ref";

            #print STDERR "The glued hash_id: $chr $pos $ref $n $b \n ";
            #print STDERR " the line: $_ \n ";
            
            ##############################################################################################################
            #### count the number on reference bases for forward and backward reads and deleting it from the sequence  ###
            ##############################################################################################################

            ## $rn set on $b // base string  ^",
            ## rn is now the sequence string
            my $rn=$b;

            ## match ,. and count it,
            ## match the reference bases we count
            ## delete them afterwards and save them in $rn
            ## ^ read start
            ## $rn is the number of bases $b matched
            $rn=~s/[^\.\,]+//g;
            
            # s/ match one line
            ## rn is now a number
            $rn=length $rn;
                    
            ## max base not set so its "-"
            my $maxb="-";
            ## temporaly max is $rn
            my $max=$rn;
            $n=$rn;
            ###############################################
            ### Count nucleotide A forward and backward ###
            ###############################################

            # a is the sequence string again
            my $a=$b;
            #print STDERR "pre match $a\n";
            # a gets a number again (amount of aA at that position)
            $a=~s/[^Aa]+//g;
            #print STDERR "post match $a\n";
            
            $a=length $a;
            $n+=$a;
			#print STDERR "number: $a\n";
            
            
            ## cornercase if reference also [Aa] u need to increase $a by the amount of $rn from before
            ## shouldnt even be possible 
            $a+=$rn if $ref=~/^[aA]$/;
            
            if($a>=$max && $ref!~/^[aA]$/ && $n != 0) { $maxb="A"; $max=$a; }
    
            my $c=$b;
            $c=~s/[^Cc]+//g;

            $c=length $c;
            $n+=$c; 
            ## cornercase if reference also [cC] u need to increase $a by the amount of $rn from before
            $c+=$rn if $ref=~/^[cC]$/;

            if($c>=$max && $ref!~/^[cC]$/ && $n != 0) { $maxb="C"; $max=$c; }
    
            my $g=$b;
            $g=~s/[^Gg]+//g;
            $g=length $g;
            $n+=$g;
            ## cornercase if reference also [gG] u need to increase $a by the amount of $rn from before
            $g+=$rn if $ref=~/^[gG]$/;
            
            if($g>=$max && $ref!~/^[gG]$/ && $n != 0) { $maxb="G"; $max=$g; }
    
            my $t=$b;
            $t=~s/[^Tt]+//g;

            $t=length $t;
            $n+=$t;
            ## cornercase if reference also [tT] u need to increase $a by the amount of $rn from before
            $t+=$rn if $ref=~/^[tT]$/;
            
            if($t>=$max && $ref!~/^[tT]$/ && $n != 0) { $maxb="T"; $max=$t; }
    
            # print always each line since we specified a specified panel from the start
            # check chromosome and starting pos and annotate the info from  the panel afterwards print it out

            ######################
            # save pipeup here ###
            ######################
            
            #save pileup columns here mit hash_id
            
			$chr_h{$hash_id}=$chr;
            
    		$pos_h{$hash_id}=$pos;
    		
    		$ref_h{$hash_id}=$ref;
    		
    		$a_h{$hash_id}=$a;
    		
    		$c_h{$hash_id}=$c;
    		
    		$g_h{$hash_id}=$g;
    		
    		$t_h{$hash_id}=$t;
    		
    		$n_h{$hash_id}=$n;
    		
    		$b_h{$hash_id}=$b;
    		
    		$q_h{$hash_id}=$q;
    		
   	  		$rn_h{$hash_id}=$rn;
    		
    		$maxb_h{$hash_id}=$maxb;
    		
    		$max_h{$hash_id}=$max;
    	}
        ### only for testing purposes!!!!
        #last if $i++>=10000;
    }
    close($fp);
    print STDERR "[INFO] ",scalar(localtime),": Samtools mpileup done.\n" if $verbose;
    
    # afterwards us the pileup of all positions and annotate them
    open my $p, '<', $input_panel or die "Can't read in $input_panel. ";

    @line = ();
    # first line
    $header = <$p>;

    # panel files is assumed to be in 1,2,3 chromosme names, nothing with chr1 etc.
    print STDERR "[INFO] ",scalar(localtime),": Preprocessing panel file.\n" if $verbose;
    print "chr\tpos\tn\tA\tC\tG\tT\tcons\tmax\tvariant_found\tref\tAlt_Allel\tGeneSymbol\tp_variant\tc_variant";
    print "\tSource\tDrug\n";    
    while (<$p>){
        chomp;
        @line = Text::ParseWords::parse_line(',',0, $_);
        next if /^\</; # no < at the begining
        next if /^\s*$/; #empty ones
        #save chromosome,start pos,ref of each variant and use it as hash id
        $hash_id="$line[3]:$line[4]:$line[6]";
        $genes{$hash_id} = $line[0]; # gene names
        $p_variant{$hash_id} = $line[1]; # variant in aminoacids
        $c_variant{$hash_id} = $line[2]; # variants in nucleotides
        $chrom{$hash_id} = $line[3];
        $chromStart{$hash_id} = $line[4];
        $chromEnd{$hash_id} = $line[5];
        $ref_allel{$hash_id} = $line[6];
        $alt_allel{$hash_id} = $line[7];
        $source{$hash_id} = $line[8];
        $drug{$hash_id} = $line[9];
            my $found="NO";    
            if(exists $genes{$hash_id}){
                # easy case if alternative allele is the highest allel
                # pay attention if we got non single nucleotide variations we get empty values "" and they match
                # we get a found yes then therefore double check this , if threshold is overcome
                # possible corner case if the maxb_h  hash and corresponding others do not have a matching hash_id for the panel
                # because the alignment file may been subsampled (like the example file >.<) or anyother reason that position is not
                # present, therefore we need to check for undef before making comparison or it will results in error warnings 
                if(( $maxb_h{$hash_id} eq $alt_allel{$hash_id}) && ($maxb_h{$hash_id} ne "" )&& ($max_h{$hash_id} >=$treshhold)){
                    $found="YES";
                    # the reference allel is the highest allel but the alternative needs to be checked still
                    # if it suffice the count_trehshold && perc_treshold -> if so found = YES
                    # alt allel cant be equal to ref allel 
                    # so the case where for example  maxb is . but alt is A and A is reference allel aswell
                    # should not be possible 
                    #
                    # we found a suboptimal (not maximum amount of reads of all bases) mutation here, 
                    # but the current alternative allel still got the highest amount of reads
                    # therefore we need to change max b to the alt allel if the alt allel got the requirements met
                    # set max read to the found allel:
                    
                    }elsif($maxb_h{$hash_id} eq "-" && $alt_allel{$hash_id} eq "A" ){
                        if($a_h{$hash_id} >=$treshhold && $a_h{$hash_id}/$n_h{$hash_id} >= $perc_treshhold){$found="YES";$maxb_h{$hash_id}="A";$max_h{$hash_id}=$a_h{$hash_id};}
                    }elsif($maxb_h{$hash_id} eq "-" && $alt_allel{$hash_id} eq "C" ){
                        if($c_h{$hash_id} >=$treshhold && $c_h{$hash_id}/$n_h{$hash_id} >= $perc_treshhold){$found="YES";$maxb_h{$hash_id}="C";$max_h{$hash_id}=$c_h{$hash_id};}
                    }elsif($maxb_h{$hash_id} eq "-" && $alt_allel{$hash_id} eq "G" ){
                        if($g_h{$hash_id} >=$treshhold && $g_h{$hash_id}/$n_h{$hash_id} >= $perc_treshhold){$found="YES";$maxb_h{$hash_id}="G";$max_h{$hash_id}=$g_h{$hash_id};}
                    }elsif($maxb_h{$hash_id} eq "-" && $alt_allel{$hash_id} eq "T" ){
                        if($t_h{$hash_id} >=$treshhold && $t_h{$hash_id}/$n_h{$hash_id} >= $perc_treshhold){$found="YES";$maxb_h{$hash_id}="T";;$max_h{$hash_id}=$t_h{$hash_id};}
                    }
                    print "$chr_h{$hash_id}\t$pos_h{$hash_id}\t$n_h{$hash_id}\t$a_h{$hash_id}\t$c_h{$hash_id}\t$g_h{$hash_id}\t$t_h{$hash_id}\t$maxb_h{$hash_id}\t$max_h{$hash_id}";
                    print "\t$found\t$ref_allel{$hash_id}\t$alt_allel{$hash_id}\t$genes{$hash_id}\t$p_variant{$hash_id}\t$c_variant{$hash_id}\t$source{$hash_id}\t$drug{$hash_id}\n";
            }else{
                print "$chr_h{$hash_id}\t$pos_h{$hash_id}\t$n_h{$hash_id}\t$a_h{$hash_id}\t$c_h{$hash_id}\t$g_h{$hash_id}\t$t_h{$hash_id}\t$maxb_h{$hash_id}\t";
                print "$max_h{$hash_id}\t$found\t$ref_allel{$hash_id}\t-\t-\t-\t-\t-\t-\n";
            }   
    }
    close($p);
    print  STDERR "[INFO] ",scalar(localtime),": Done.\n" if $verbose;

}else{ ## complete mode

    # var annotation hash should have all possible combined db findings for the mutation called
    # by pileup
    # multiple cases to process
    #  111 1. all three databases got entries for that found
    #  011 2. one each didnt find a annotation
    #  101
    #  110
    #  100 3. only one found an annotation
    #  010
    #  001
    #  000 4. none of them found an annotation
    #  This structure reflected in the var_annotation_hash
    my %var_annotation_hash= ();
    
    ##############################
    # ClinVar Annotation parsing #
    ##############################

    my $input_file = $clinvar;
    open my $fh, '<', $input_file or die print_options();
    # first line
    $header = <$fh>;
    my $i=0;
    ## verbose ##
    print STDERR "[INFO] ",scalar(localtime),": Reading $clinvar.\n" if $verbose;



    #############
    while (<$fh>){
        chomp;
        next if /^\</; # no < at the begining
        next if /^\s*$/; #empty ones
        @db_select=(split /\t/, $_)[1,2,4,6,9,10,13,14,18,19,20,21,22];


        #Type, Name,GeneSymbol,ClinSig,dnsnp, dbvar,PhenotypeList, Origin,Chr, START, STOP, Ref Allel, Alt Allel
        #print Dumper \@db_select;
        $saved_entries_clinvar[$i][0] = $db_select[0]; #Type,
        $saved_entries_clinvar[$i][1] = $db_select[1]; #Name,
        $saved_entries_clinvar[$i][2] = $db_select[2]; #GeneSymbol
        $saved_entries_clinvar[$i][3] = $db_select[3]; #ClinSig
        $saved_entries_clinvar[$i][4] = $db_select[4]; #dbsnp,
        $saved_entries_clinvar[$i][5] = $db_select[5]; #dbvar,
        $saved_entries_clinvar[$i][6] = $db_select[6]; #PhenotypeList,
        $saved_entries_clinvar[$i][7] = $db_select[7]; #Origin
        $saved_entries_clinvar[$i][8] = $db_select[8]; #Chr,
        $saved_entries_clinvar[$i][9] = $db_select[9]; #START,
        $saved_entries_clinvar[$i][10] = $db_select[10]; #STOP,
        $saved_entries_clinvar[$i][11] = $db_select[11]; #ref allel,
        $saved_entries_clinvar[$i][12] = $db_select[12]; #alt allel,
        # fill up the lookup hash in parallel
        # total of 12 entries,$db_select[10]; #STOP,we skipped
        $var_annotation_hash{"$db_select[8]:$db_select[9]:$db_select[11]:$db_select[12]"} ="$db_select[0]\t$db_select[1]\t$db_select[2]\t$db_select[3]\t$db_select[4]\t$db_select[5]\t$db_select[6]\t$db_select[7]\t$db_select[8]\t$db_select[9]\t$db_select[11]\t$db_select[12]"; 
        $i++;
    }
    close($fh);
    print STDERR "[INFO] ",scalar(localtime),": Done.\n" if $verbose;

    #############################
    # cosmic annotation parsing #
    #############################

    my $input_file_cos = $cosmic;
    open my $fhc, '<', $input_file_cos or die "Can't read in $input_file_cos. ";
    # first line
    $header = <$fhc>;
    my $j=0;
    my $to_compare=(); #string  chr:pos:ref:alt for hash lookup
    my $to_add=(); #string of values to to the hash key
   
    #Type, Name,GeneSymbol,ClinSig,dnsnp, dbvar,PhenotypeList, Origin,Chr, START, STOP, Ref Allel, Alt Allel
    my $not_in_clinvar="-\t-\t-\t-\t-\t-\t-\t-\t-\t-\t-\t-"; #12
    ## verbose ##
    print STDERR "[INFO] ",scalar(localtime),": Reading $cosmic.\n" if $verbose;
    #############
    while (<$fhc>){
        chomp;
        next if /^\</; # no < at the beginning
        next if /^\s*$/; #empty ones
        my @subcosmic= ();
        #print STDERR "this line $_ \n";
        my @db_select=(split /\t/, $_);
        #print(@db_select);
        #values: 0 - cosmic
        #        1 - GeneName
        #        2 - chr:start-stop
        #        3 - strand
        #        4 - mutation c.1022G>T for example, needs to be reversed if strand is negative
        #        5 - primarysite
        #        6 - histology
        #        7 - mutation desc.
        #        8 - somatic information
        #        9 - pubmed   
        $saved_entries_cosmic[$j][0] = $db_select[0]; #CosmicID,
        $saved_entries_cosmic[$j][1] = $db_select[1]; #Gene_Name,
        $saved_entries_cosmic[$j][2] = (split /:/,$db_select[2])[0]; #Mutation_genome_position # chr:star-stop need to grab chr via split
        my $subselect = (split /:/,$db_select[2])[1]; # select the part after ':'
        if (defined $subselect){
	        $saved_entries_cosmic[$j][3] = (split /-/,$subselect)[0]; # is the start pos after ':' and before '-', ..)[2] would be end pos
    	    $saved_entries_cosmic[$j][4] = $db_select[3]; #Mutation_strand: important
        	@subcosmic = (split /[c]\.\d+([ACGTacgt])>([ACGTacgt])/,$db_select[4]);

            # check if the subcosmic is in the form of c.NUMBERREF>ALT otherwise skip the process, for now only SNV are supported
            # check if it is defined so it should match a single nucleotide of either ACGTacgt
           	if (defined $subcosmic[1] and defined $subcosmic[2] and $subcosmic[1] ne '' and $subcosmic[2] ne '') {
                $saved_entries_cosmic[$j][5] = $subcosmic[1]; #the first match of subcosmic, the REF allel
                $saved_entries_cosmic[$j][6] = $subcosmic[2]; #the second match of subcosmic, the ALT allel
                $saved_entries_cosmic[$j][5] = $reverseNucleotides{$subcosmic[1]} if $db_select[3] eq '-'; # cDNA from negative strand
                $saved_entries_cosmic[$j][6] = $reverseNucleotides{$subcosmic[2]} if $db_select[3] eq '-'; # cDNA from negative strand
                $saved_entries_cosmic[$j][7] = $db_select[5]; #Primary_site # occurence of tumor or NS(not specified)
                $saved_entries_cosmic[$j][8] = $db_select[6]; #Histology, histological classifcation already parsed
                $saved_entries_cosmic[$j][9] = $db_select[7]; #Mutation_Description
                $saved_entries_cosmic[$j][10] = $db_select[8];
                #Mutation_somatic_status : would be nice to get a parsing reg ex to (clearly) state somatic yes/no
                $saved_entries_cosmic[$j][11] = $db_select[9]; #Pubmed_PMID
                # geting undef for an empty pmid is ugly so set it on not specified
                if(undef ~~ $saved_entries_cosmic[$j][11]){ $saved_entries_cosmic[$j][11] ="NS"}; 
                $to_compare= "$saved_entries_cosmic[$j][2]:$saved_entries_cosmic[$j][3]:$saved_entries_cosmic[$j][5]:$saved_entries_cosmic[$j][6]";
                # if key values match chr pos and ref alt are redundant so create u subset to add
                $to_add="$saved_entries_cosmic[$j][0]\t$saved_entries_cosmic[$j][1]\t$saved_entries_cosmic[$j][7]\t$saved_entries_cosmic[$j][8]\t$saved_entries_cosmic[$j][9]\t$saved_entries_cosmic[$j][10]\t$saved_entries_cosmic[$j][11]";
                if(exists $var_annotation_hash{"$to_compare"})
                #values: 0 - cosmic
                #        1 - GeneName
                #        5 - primarysite
                #        6 - histology
                #        7 - mutation desc.
                #        8 - somatic information
                #        9 - pubmed  
                {$var_annotation_hash{"$to_compare"}= join("\t",$var_annotation_hash{"$to_compare"},$to_add)}
                else{$var_annotation_hash{"$to_compare"}=join("\t",$not_in_clinvar,$to_add);}
            }else{
               if($verbose){ warn "[INFO] No SNV, skipped entry at line $j, because it didn't matched the pattern of a variant (example: c.1022G>T).\n";}
            }
        }else{
            if($verbose){ warn "[INFO] No SNV, skipped entry at line $j, because the Mutation_genome_position is not defined.\n";} 
        }
        $j++;
    }
    close($fhc);
    print STDERR "[INFO] ",scalar(localtime),": Done.\n" if $verbose;

    # we are now missing the case were we have clinvar entry but not
    # cosmic entrys, we need to update these with "\t-\t-" etc.
    # so we preserve the order of entries in columns in the final .tsv
    my $not_in_cosmic="-\t-\t-\t-\t-\t-\t-"; #7
                #values: 0 - cosmic
                #        1 - GeneName
                #        5 - primarysite
                #        6 - histology
                #        7 - mutation desc.
                #        8 - somatic information
                #        9 - pubmed  
   
    my @clinvar_cosmic_array = keys %var_annotation_hash;
    for my $hash_keys (@clinvar_cosmic_array) {
        #check string lengths of split and update the value 
        # for the according hashkey
        # 12 elements means no cosmic find, 19 means a cosmic find
        my @n = split '\t', $var_annotation_hash{$hash_keys};
        my $count= $#n+1;
        if($count==12){
            $var_annotation_hash{$hash_keys}=join("\t",$var_annotation_hash{$hash_keys},$not_in_cosmic);
        }
        # print STDERR "$var_annotation_hash{$hash_keys}\n";
    }

    ############################
    # CIViC Annotation parsing #
    ############################


    my $input_file_civ = $civic;
    open my $fhi, '<', $input_file_civ or die "Can't read in $input_file_civ. ";
    # first line
    $header = <$fhi>;
    my $k=0;
    ## if its not known by hash lookup its neither in clinvar nor in cosmic
    my $not_in_clinvar_cosmic="-\t-\t-\t-\t-\t-\t-\t-\t-\t-\t-\t-\t-\t-\t-\t-\t-\t-\t-";
    ## verbose ##
    print STDERR "[INFO] ",scalar(localtime),": Reading $civic.\n" if $verbose;

    while (<$fhi>){
        chomp;
        next if /^\</; # no < at the beginning
        next if /^\s*$/; #empty ones
         @db_select = Text::ParseWords::parse_line(',',0, $_);
         $saved_entries_civic[$k][0] = $db_select[3]; #chr,
         $saved_entries_civic[$k][1] = $db_select[4]; #start,
         $saved_entries_civic[$k][2] = $db_select[5]; #stop 
         $saved_entries_civic[$k][3] = $db_select[6]; #ref: 
         $saved_entries_civic[$k][4] = $db_select[7]; #alt: 
         $saved_entries_civic[$k][5] = $db_select[9];  #civic summary
         #string  chr:pos:ref:alt for hash lookup
         $to_compare= "$db_select[3]:$db_select[4]:$db_select[6]:$db_select[7]";
         $to_add="$db_select[9]";
         
        if(exists $var_annotation_hash{"$to_compare"})
            {$var_annotation_hash{"$to_compare"}= join("\t",$var_annotation_hash{"$to_compare"},$to_add)}
        else{$var_annotation_hash{"$to_compare"}=join("\t",$not_in_clinvar_cosmic,$to_add);   }
        $k++;
    }
    close($fhi);
    # some idea again for civic
    my $not_in_civic="-\t";
   
    my @clinvar_cosmic_civic_array = keys %var_annotation_hash;
    for my $hash_keys (@clinvar_cosmic_civic_array) {
        #check string lengths of split and update the value 
        # for the according hashkey
        # 19 elements means no civic find, 20 means a civic find
        my @n = split '\t', $var_annotation_hash{$hash_keys};
        my $count_civic= $#n+1;
        #print STDERR "$count_civic\n $var_annotation_hash{$hash_keys}\n";
        if($count_civic==19){
            $var_annotation_hash{$hash_keys}=join("\t",$var_annotation_hash{$hash_keys},$not_in_civic);
        }
        # print STDERR "$var_annotation_hash{$hash_keys}\n";
    }
    print STDERR "[INFO] ",scalar(localtime),": Done.\n" if $verbose;

    ## -B Base alignment quality (BAQ) computation  is turned off as its not used heredisabled
    ## -I no Indel Calling performed
    ## -f faidx reference file
    ## filtering arround indels with BAQ -E option ?=

    # create a hash with key [chr:pos:ref:alt] and as values concatenated strings for all three dbs
    print STDERR "[INFO] ",scalar(localtime),": Reading and pileuping $aligned.\n" if $verbose;
#    open my $F;
   if($custom_tracks ne ""){
	open( FILE,"$samtools mpileup -I -B -a -l $custom_tracks -f $ref $aligned |");

		    # -B disable BAQ computation
    # -I do not perform indel calling
    ## example output
    # $chr     $pos   $ref    $n       $b
    ## 1       10541   C       1       ^",     D
    ## 1       10542   C       2       ,^",    DA

    ## Annotation to the matching sheme in $b

    ##  At the read base column,
    ## .  a dot stands for a match to the reference base on the forward strand,
    ## ,  a comma for a match on the reverse strand,
    ## `ACGTN' for a mismatch on the forward strand and
    ## `acgtn' for a mismatch on the reverse strand. A pattern
    ## `\+[0    -9]+[ACGTNacgtn]+' indicates there is an insertion between this reference position and the next reference position. The length of the insertion is given by the integer in the
    ## 			   pattern, followed by the inserted sequence. Here is an example of 2bp insertions on three reads: seq2 156 A 11  .$......+2AG.+2AG.+2AGGG    <975;:<<<<<
    ## -[0-9]+[ACGTNacgtn]+'   represents a deletion from the reference. Here is an example of a 4bp deletions from the reference, supported by two reads:
    ##			   seq3 200 A 20 ,,,,,..,.-4CACC.-4CACC....,.,,.^~. ==<<<<<<<<<<<::<;2<<
    ## `^'			   marks the start of a read segment which is a contiguous subsequence on the read separated by `N/S/H' CIGAR operations
    ## `$' 			   marks the end of a read segment.

    print "chr\tpos\tn\tref\tA\tC\tG\tT\tcons\tmax\tType\tName\tGeneSymbol\tClinSig\tdbsnp\tdbvar\tPhenotypeList\tOrigin\tChr\tSTART";
    print "\tRef_Allel\tAlt_Allel\tcosmic\tGeneName\tprimarysite\thistology\tmutation_desc\tsomatic information\tpubmed\tcivic_summary\n";
	    while(<FILE>) {

	        ## ^ 	Start of line
	        ## \w 	Any word character (letter, number, underscore)
	        ## \d 	Any digit
	        ## \S 	Any non-whitespace character
	        ## \s 	Any whitespace character
	        if(/^(\w+)\t(\d+)\t(\w+)\t(\d+)\t(\S+)\t(\S+)\s*$/){
	            ## saving meta information of the line
	            ## chromosome name
	            my $chr=$1;
	           
	            ## reference nucleotide position /1-based coordinate
	            my $pos=$2;

	            ## reference nucleotide /the number of reads covering the site
	            my $ref=$3;

	            ## readdepth at the position
	   	        # seems also to count stuff which is not reference or alternativ allel from time to time, 
	   	        # therefore i have to count it myself otherwise a,c,g,t would not sum up to n
	   	        # ill overwrite it later on
	   	        my $n=$4;

	            ## read bases
	    	    my $b=$5;

	            ## base qualities [not utilized yet]
	            my $q=$6;

	            ## number of reads supporting the position/nucleotide[ACGT]
	    	    my $evidence=0;
	            #$chr=~ s/chr//i;
	    	    ##############################################################################################################
	    	    #### count the number on reference bases for forward and backward reads and deleting it from the sequence  ###
	    	    ##############################################################################################################

	            ## $rn set on $b // base string  ^",
	            ## rn is now the sequence string
	            my $rn=$b;
	            
	            ## match ,. and count it,
	            ## match the reference bases we count
	            ## delete them afterwards and save them in $rn
	            ## ^ read start
	            ## $rn is the number of bases $b matched

	            # s/ match one line
	            $rn=~s/[^\.\,]+//g;
	            # print STDERR  "$rn\n"; 
	            ## rn is now a number
	            $rn=length $rn;
	            $n= $rn;
	            #print STDERR  "$rn\n"; 
	                    
	            ## max base not set so its "-"
	            my $maxb="-";
	            ## temporaly max is $rn
	            my $max=$rn;

	            #print STDERR  "$max\n";
	            
	            ###############################################
	            ### Count nucleotide A forward and backward ###
	            ###############################################

	            # a is the sequence string again
	            my $a=$b;
				
				# a gets a number again (amount of aA at that position)
	            $a=~s/[^Aa]+//g;
	            $a=length $a;
	            $n+=$a;
	            ## cornercase if reference also [Aa] u need to increase $a by the amount of $rn from before
	            ## shouldnt even be possible 
	            $a+=$rn if $ref=~/^[aA]$/;
	            
	            # issue cornercase if all alt nucleotide and/or the ref have the same amount
	            # alt allel would be always t or the last nucleotide it matched
	            if($a>=$max && $ref!~/^[aA]$/ && $n != 0) { $maxb="A"; $max=$a; $evidence=$a; }
	            
	            my $c=$b;
	            $c=~s/[^Cc]+//g;

	            $c=length $c;
	         	$n+=$c;
	            ## cornercase if reference also [cC] u need to increase $a by the amount of $rn from before
	            $c+=$rn if $ref=~/^[cC]$/;

	            if($c>=$max && $ref!~/^[cC]$/ && $n != 0) { $maxb="C"; $max=$c; $evidence=$c;}
	            

	            my $g=$b;
	            $g=~s/[^Gg]+//g;
	            $g=length $g;
	            $n+=$g;
	            ## cornercase if reference also [gG] u need to increase $a by the amount of $rn from before
	            $g+=$rn if $ref=~/^[gG]$/;
	            
	            if($g>=$max && $ref!~/^[gG]$/ && $n != 0) { $maxb="G"; $max=$g; $evidence=$g;}
	            

	            my $t=$b;
	            $t=~s/[^Tt]+//g;

	            $t=length $t;
	            $n+=$t;
	            ## cornercase if reference also [tT] u need to increase $a by the amount of $rn from before
	            $t+=$rn if $ref=~/^[tT]$/;
	            
	            if($t>= $max && $ref!~/^[tT]$/ && $n != 0) { $maxb="T"; $max=$t; $evidence=$t;}
	            
	            #TODO: this only sets maxb to an alternative allel if one of the nucleotides has a equal or higher number then the reference allel
	            # allelic fractions < 50% are then not considered yet, so check before pushing to varfound if maxb is still "-", lookup the highest reference allel
	            # and set maxb to it, if the absolute count is higher then threshold or the relative % higher then the $perc_treshold

	            # since  $rn was set based on the amount of ,. in the beginning which are the number of reference alles at the start 
	            # if the total amount of variant nucleotides (evidence) is bigger then a threshold or a certain percentage
	            # it will nevertheless be put to varfound if the combined amount of variant nucleotides is bigger then the treshold 
	            # evidence is the combined amount of variant nucleotides, but maxb would still theless be put to varfound if the combined amount of variant nucleotides is bigger then the treshold 
	            # evidence is the combined amount of variant nucleotides, but maxb would still be "-" and therefore no annotation could match
	            # if not at least more alternative nucleotides of either A,C,G or T  exist then the reference, otherwise maxb is not set to an alternative 
	            # base and therefore the $varfound could not be matched!
	            
	            # check if maxb == "-" -> highest counter is reference but check now if we got a,c,g,t exceding the treshold, take the highest one
	            if( $maxb eq "-" && $max !=0){
	            	#seems to take the first max value if all values are equal
	            	# [0][0] is actual value
	            	# [0][1] is position of value
	            	my $max_nuc="-";
	            	if ($ref eq "A"){
	            		my @cgt_max_posmax = max ([$c,$g,$t]);
	            		# if any of these three statements is true we should declare it as a reference Allel
	            		if($cgt_max_posmax[0][0] == 0 || ($cgt_max_posmax[0][0] < $treshhold || $cgt_max_posmax[0][0]/$n < $perc_treshhold )){	
	            			$max_nuc= "-";
							push @varfound, "$chr\t$pos\t$n\t$ref\t$a\t$c\t$g\t$t\t$max_nuc\t$max\n" ;
	        	 		}else{
							$max_nuc= "C" if  $cgt_max_posmax[0][1] == 0 ;
	            			$max_nuc= "G" if  $cgt_max_posmax[0][1] == 1 ;
	            			$max_nuc= "T" if  $cgt_max_posmax[0][1] == 2 ;
	            		# only push the variant if the max number of nucleotiedes exceeds the threshold [default 3 reads] and the percent treshold [default 0.10]
	        			#print STDERR Dumper "$acgt_max_posmax[0][0]\n";
	            			push @varfound, "$chr\t$pos\t$n\t$ref\t$a\t$c\t$g\t$t\t$max_nuc\t$cgt_max_posmax[0][0]\n" ;}
	            	 }elsif($ref eq "C"){
	            		my @agt_max_posmax = max ([$a,$g,$t]);
						if($agt_max_posmax[0][0] == 0 || ($agt_max_posmax[0][0] < $treshhold || $agt_max_posmax[0][0]/$n < $perc_treshhold )){
							$max_nuc= "-";
							push @varfound, "$chr\t$pos\t$n\t$ref\t$a\t$c\t$g\t$t\t$max_nuc\t$max\n" ;}
						else{
	            			$max_nuc= "A" if  $agt_max_posmax[0][1] == 0 ;
	            			$max_nuc= "G" if  $agt_max_posmax[0][1] == 1 ;
	            			$max_nuc= "T" if  $agt_max_posmax[0][1] == 2 ;
	            			push @varfound, "$chr\t$pos\t$n\t$ref\t$a\t$c\t$g\t$t\t$max_nuc\t$agt_max_posmax[0][0]\n" ;}
	            	# only push the variant if the max number of nucleotiedes exceeds the threshold [default 3 reads] and the percent treshold [default 0.10]
	            	#print STDERR Dumper "$acgt_max_posmax[0][0]\n";
	            	
	            	 }elsif($ref eq "G") {
	            		my @act_max_posmax = max ([$a,$c,$t]);
						if($act_max_posmax[0][0] == 0 || ($act_max_posmax[0][0] < $treshhold || $act_max_posmax[0][0]/$n < $perc_treshhold )){
							$max_nuc= "-";
							push @varfound, "$chr\t$pos\t$n\t$ref\t$a\t$c\t$g\t$t\t$max_nuc\t$max\n" ;}
	        	 		else{
	              			$max_nuc= "A" if  $act_max_posmax[0][1] == 0 ;
	            			$max_nuc= "C" if  $act_max_posmax[0][1] == 1 ;
	            			$max_nuc= "T" if  $act_max_posmax[0][1] == 2 ;
	            			push @varfound, "$chr\t$pos\t$n\t$ref\t$a\t$c\t$g\t$t\t$max_nuc\t$act_max_posmax[0][0]\n" ;}
	            	# only push the variant if the max number of nucleotiedes exceeds the threshold [default 3 reads] and the percent treshold [default 0.10]
	            	#print STDERR Dumper "$acgt_max_posmax[0][0]\n";
	            	
	            	 }elsif($ref eq "T") {
	            		my @acg_max_posmax = max ([$a,$c,$g]);
	            		if(	$acg_max_posmax[0][0] == 0 || ($acg_max_posmax[0][0] < $treshhold || $acg_max_posmax[0][0]/$n < $perc_treshhold )){
							$max_nuc= "-";
							push @varfound, "$chr\t$pos\t$n\t$ref\t$a\t$c\t$g\t$t\t$max_nuc\t$max\n" ;}
	        	 	    else{
	            			$max_nuc= "A" if  $acg_max_posmax[0][1] == 0 ;
	            			$max_nuc= "C" if  $acg_max_posmax[0][1] == 1 ;
	            			$max_nuc= "G" if  $acg_max_posmax[0][1] == 2 ;
							push @varfound, "$chr\t$pos\t$n\t$ref\t$a\t$c\t$g\t$t\t$max_nuc\t$acg_max_posmax[0][0]\n" ;}
	            	# only push the variant if the max number of nucleotiedes exceeds the threshold [default 3 reads] and the percent treshold [default 0.10]
	            	#print STDERR Dumper "$acgt_max_posmax[0][0]\n";
	            	
	            	#print  STDERR Dumper "my local max is : $acgt_max_posmax[0][1]\n";
	            	# take maximum of a,c,g,t, set max to the nucleotide and max to the amount of these
	            	# what to do if it ties? is there a max function in perl? how does this function handle it??
	            	# push varfound
	            #else maxb could be - but max is 0 that means we have no reads at all so it would not surpass the treshold and not pushed
	            # or its not - and an easy case of number of alt nucleotides > ref and is pushed correctly
	       		#push @varfound, "$chr\t$pos\t$n\t$ref\t$a\t$c\t$g\t$t\t$maxb\t$max\n" if $evidence>= $treshhold && $evidence/$n >= $perc_treshhold;
	    			}
	            }else{     
	            	push @varfound, "$chr\t$pos\t$n\t$ref\t$a\t$c\t$g\t$t\t$maxb\t$max\n" ;
	        	}
	        }

	    }
    close(FILE);
    print STDERR "[INFO] ",scalar(localtime),": Done.\n" if $verbose;

    print STDERR "[INFO] ",scalar(localtime),": Annotating variants and printing results to STDOUT.\n" if $verbose;
    print STDERR "[INFO] ",scalar(localtime),": Skip_variants option is active, only printing annotated variants.\n" if ($verbose && $skip_variants)  ;
    
	}else{
		open( FILE,"$samtools mpileup -I -B -f $ref $aligned |");
		    # -B disable BAQ computation
	
    # -I do not perform indel calling
    ## example output
    # $chr     $pos   $ref    $n       $b
    ## 1       10541   C       1       ^",     D
    ## 1       10542   C       2       ,^",    DA

    ## Annotation to the matching sheme in $b

    ##  At the read base column,
    ## .  a dot stands for a match to the reference base on the forward strand,
    ## ,  a comma for a match on the reverse strand,
    ## `ACGTN' for a mismatch on the forward strand and
    ## `acgtn' for a mismatch on the reverse strand. A pattern
    ## `\+[0    -9]+[ACGTNacgtn]+' indicates there is an insertion between this reference position and the next reference position. The length of the insertion is given by the integer in the
    ## 			   pattern, followed by the inserted sequence. Here is an example of 2bp insertions on three reads: seq2 156 A 11  .$......+2AG.+2AG.+2AGGG    <975;:<<<<<
    ## -[0-9]+[ACGTNacgtn]+'   represents a deletion from the reference. Here is an example of a 4bp deletions from the reference, supported by two reads:
    ##			   seq3 200 A 20 ,,,,,..,.-4CACC.-4CACC....,.,,.^~. ==<<<<<<<<<<<::<;2<<
    ## `^'			   marks the start of a read segment which is a contiguous subsequence on the read separated by `N/S/H' CIGAR operations
    ## `$' 			   marks the end of a read segment.

    print "chr\tpos\tn\tref\tA\tC\tG\tT\tcons\tmax\tType\tName\tGeneSymbol\tClinSig\tdbsnp\tdbvar\tPhenotypeList\tOrigin\tChr\tSTART";
    print "\tRef_Allel\tAlt_Allel\tcosmic\tGeneName\tprimarysite\thistology\tmutation_desc\tsomatic information\tpubmed\tcivic_summary\n";
	    while(<FILE>) {

	        ## ^ 	Start of line
	        ## \w 	Any word character (letter, number, underscore)
	        ## \d 	Any digit
	        ## \S 	Any non-whitespace character
	        ## \s 	Any whitespace character
	        if(/^(\w+)\t(\d+)\t(\w+)\t(\d+)\t(\S+)\t(\S+)\s*$/) {
	            ## saving meta information of the line
	            ## chromosome name
	            my $chr=$1;
	           
	            ## reference nucleotide position /1-based coordinate
	            my $pos=$2;

	            ## reference nucleotide /the number of reads covering the site
	            my $ref=$3;

	            ## readdepth at the position
	   	        # seems also to count stuff which is not reference or alternativ allel from time to time, 
	   	        # therefore i have to count it myself otherwise a,c,g,t would not sum up to n
	   	        # ill overwrite it later on
	   	        my $n=$4;

	            ## read bases
	    	    my $b=$5;

	            ## base qualities [not utilized yet]
	            my $q=$6;

	            ## number of reads supporting the position/nucleotide[ACGT]
	    	    my $evidence=0;
	            #$chr=~ s/chr//i;
	    	    ##############################################################################################################
	    	    #### count the number on reference bases for forward and backward reads and deleting it from the sequence  ###
	    	    ##############################################################################################################

	            ## $rn set on $b // base string  ^",
	            ## rn is now the sequence string
	            my $rn=$b;
	            
	            ## match ,. and count it,
	            ## match the reference bases we count
	            ## delete them afterwards and save them in $rn
	            ## ^ read start
	            ## $rn is the number of bases $b matched

	            # s/ match one line
	            $rn=~s/[^\.\,]+//g;
	            # print STDERR  "$rn\n"; 
	            ## rn is now a number
	            $rn=length $rn;
	            $n= $rn;
	            #print STDERR  "$rn\n"; 
	                    
	            ## max base not set so its "-"
	            my $maxb="-";
	            ## temporaly max is $rn
	            my $max=$rn;

	            #print STDERR  "$max\n";
	            
	            ###############################################
	            ### Count nucleotide A forward and backward ###
	            ###############################################

	            # a is the sequence string again
	            my $a=$b;
				
				# a gets a number again (amount of aA at that position)
	            $a=~s/[^Aa]+//g;
	            $a=length $a;
	            $n+=$a;
	            ## cornercase if reference also [Aa] u need to increase $a by the amount of $rn from before
	            ## shouldnt even be possible 
	            $a+=$rn if $ref=~/^[aA]$/;
	            
	            # issue cornercase if all alt nucleotide and/or the ref have the same amount
	            # alt allel would be always t or the last nucleotide it matched
	            if($a>=$max && $ref!~/^[aA]$/ && $n != 0) { $maxb="A"; $max=$a; $evidence=$a; }
	            
	            my $c=$b;
	            $c=~s/[^Cc]+//g;

	            $c=length $c;
	         	$n+=$c;
	            ## cornercase if reference also [cC] u need to increase $a by the amount of $rn from before
	            $c+=$rn if $ref=~/^[cC]$/;

	            if($c>=$max && $ref!~/^[cC]$/ && $n != 0) { $maxb="C"; $max=$c; $evidence=$c;}
	            

	            my $g=$b;
	            $g=~s/[^Gg]+//g;
	            $g=length $g;
	            $n+=$g;
	            ## cornercase if reference also [gG] u need to increase $a by the amount of $rn from before
	            $g+=$rn if $ref=~/^[gG]$/;
	            
	            if($g>=$max && $ref!~/^[gG]$/ && $n != 0) { $maxb="G"; $max=$g; $evidence=$g;}
	            

	            my $t=$b;
	            $t=~s/[^Tt]+//g;

	            $t=length $t;
	            $n+=$t;
	            ## cornercase if reference also [tT] u need to increase $a by the amount of $rn from before
	            $t+=$rn if $ref=~/^[tT]$/;
	            
	            if($t>= $max && $ref!~/^[tT]$/ && $n != 0) { $maxb="T"; $max=$t; $evidence=$t;}
	            
	            #TODO: this only sets maxb to an alternative allel if one of the nucleotides has a equal or higher number then the reference allel
	            # allelic fractions < 50% are then not considered yet, so check before pushing to varfound if maxb is still "-", lookup the highest reference allel
	            # and set maxb to it, if the absolute count is higher then threshold or the relative % higher then the $perc_treshold

	            # since  $rn was set based on the amount of ,. in the beginning which are the number of reference alles at the start 
	            # if the total amount of variant nucleotides (evidence) is bigger then a threshold or a certain percentage
	            # it will nevertheless be put to varfound if the combined amount of variant nucleotides is bigger then the treshold 
	            # evidence is the combined amount of variant nucleotides, but maxb would still theless be put to varfound if the combined amount of variant nucleotides is bigger then the treshold 
	            # evidence is the combined amount of variant nucleotides, but maxb would still be "-" and therefore no annotation could match
	            # if not at least more alternative nucleotides of either A,C,G or T  exist then the reference, otherwise maxb is not set to an alternative 
	            # base and therefore the $varfound could not be matched!
	            
	            # check if maxb == "-" -> highest counter is reference but check now if we got a,c,g,t exceding the treshold, take the highest one
	            if(( $maxb eq "-" && $max !=0)){
	            	#takes the first max value if [0,1,2] got all the same counts it will always take the first one
	            	# [0][0] is actual value
	            	# [0][1] is position of value
	            	if ($ref eq "A") {
	            		my @cgt_max_posmax = max ([$c,$g,$t]);
	            		my $max_nuc= "-";
	            		$max_nuc= "C" if  $cgt_max_posmax[0][1] == 0 ;
	            		$max_nuc= "G" if  $cgt_max_posmax[0][1] == 1 ;
	            		$max_nuc= "T" if  $cgt_max_posmax[0][1] == 2 ;
	            	# only push the variant if the max number of nucleotiedes exceeds the threshold [default 3 reads] and the percent treshold [default 0.10]
	            	#print STDERR Dumper "$acgt_max_posmax[0][0]\n";
	            	push @varfound, "$chr\t$pos\t$n\t$ref\t$a\t$c\t$g\t$t\t$max_nuc\t$cgt_max_posmax[0][0]\n" if $cgt_max_posmax[0][0] >= $treshhold && $cgt_max_posmax[0][0]/$n >= $perc_treshhold;
	        	 	
	            	 }elsif($ref eq "C") {
	            		my @agt_max_posmax = max ([$a,$g,$t]);
	            		my $max_nuc= "-";
	            		$max_nuc= "A" if  $agt_max_posmax[0][1] == 0 ;
	            		$max_nuc= "G" if  $agt_max_posmax[0][1] == 1 ;
	            		$max_nuc= "T" if  $agt_max_posmax[0][1] == 2 ;
	            	# only push the variant if the max number of nucleotiedes exceeds the threshold [default 3 reads] and the percent treshold [default 0.10]
	            	#print STDERR Dumper "$acgt_max_posmax[0][0]\n";
	            	push @varfound, "$chr\t$pos\t$n\t$ref\t$a\t$c\t$g\t$t\t$max_nuc\t$agt_max_posmax[0][0]\n" if $agt_max_posmax[0][0] >= $treshhold && $agt_max_posmax[0][0]/$n >= $perc_treshhold;
	        	 	
	            	 }elsif($ref eq "G") {
	            		my @act_max_posmax = max ([$a,$c,$t]);
	            		my $max_nuc= "-";
	            		$max_nuc= "A" if  $act_max_posmax[0][1] == 0 ;
	            		$max_nuc= "C" if  $act_max_posmax[0][1] == 1 ;
	            		$max_nuc= "T" if  $act_max_posmax[0][1] == 2 ;
	            	# only push the variant if the max number of nucleotiedes exceeds the threshold [default 3 reads] and the percent treshold [default 0.10]
	            	#print STDERR Dumper "$acgt_max_posmax[0][0]\n";
	            	push @varfound, "$chr\t$pos\t$n\t$ref\t$a\t$c\t$g\t$t\t$max_nuc\t$act_max_posmax[0][0]\n" if $act_max_posmax[0][0] >= $treshhold && $act_max_posmax[0][0]/$n >= $perc_treshhold;
	        	 	
	            	 }elsif($ref eq "T") {
	            		my @acg_max_posmax = max ([$a,$c,$g]);
	            		my $max_nuc= "-";
	            		$max_nuc= "A" if  $acg_max_posmax[0][1] == 0 ;
	            		$max_nuc= "C" if  $acg_max_posmax[0][1] == 1 ;
	            		$max_nuc= "G" if  $acg_max_posmax[0][1] == 2 ;
	            	# only push the variant if the max number of nucleotiedes exceeds the threshold [default 3 reads] and the percent treshold [default 0.10]
	            	#print STDERR Dumper "$acgt_max_posmax[0][0]\n";
	            	push @varfound, "$chr\t$pos\t$n\t$ref\t$a\t$c\t$g\t$t\t$max_nuc\t$acg_max_posmax[0][0]\n" if $acg_max_posmax[0][0] >= $treshhold && $acg_max_posmax[0][0]/$n >= $perc_treshhold;
	        	 	
	            	 }
	            	
	            	#print  STDERR Dumper "my local max is : $acgt_max_posmax[0][1]\n";
	            	# take maximum of a,c,g,t, set max to the nucleotide and max to the amount of these
	            	# what to do if it ties? is there a max function in perl? how does this function handle it??
	            	# push varfound
	            #else maxb could be - but max is 0 that means we have no reads at all so it would not surpass the treshold and not pushed
	            # or its not - and an easy case of number of alt nucleotides > ref and is pushed correctly
	            }else{     
	            	push @varfound, "$chr\t$pos\t$n\t$ref\t$a\t$c\t$g\t$t\t$maxb\t$max\n" if $max>= $treshhold && $max/$n >= $perc_treshhold;
	        	}
	        }

	    }
	    close(FILE);
    print STDERR "[INFO] ",scalar(localtime),": Done.\n" if $verbose;

    print STDERR "[INFO] ",scalar(localtime),": Annotating variants and printing results to STDOUT.\n" if $verbose;
    print STDERR "[INFO] ",scalar(localtime),": Skip_variants option is active, only printing annotated variants.\n" if ($verbose && $skip_variants)  ;
    

   }
    

    my $line_counter =0;
    my $varlookup="";
    foreach my $line (@varfound){
      chomp $line; # chomp \n from $line
      
      # select all entrys in varfound for later use
      # chop away \n
      my ($chr_var,$pos_var,$n,$ref,$a,$c,$g,$t,$maxb,$max) = split(/\t/, $line);
      #check the entries in the db for machtes
      #$chr_var =~ s/chr//i;
      $varlookup= "$chr_var:$pos_var:$ref:$maxb";
      if(exists $var_annotation_hash{"$varlookup"}){print("$chr_var\t$pos_var\t$n\t$ref\t$a\t$c\t$g\t$t\t$maxb\t$max\t$var_annotation_hash{$varlookup}\n") ; }
      else{ print("$chr_var\t$pos_var\t$n\t$ref\t$a\t$c\t$g\t$t\t$maxb\t$max\t$not_in_clinvar_cosmic\t-\n") if $skip_variants==0 ;}
      	$line_counter=$line_counter+1;
      	print STDERR "[INFO] ",scalar(localtime),": Mutations $line_counter of ",scalar @varfound," processed. \n" if $line_counter % 1000 == 0 ;
    }
    
    print STDERR "[INFO] ",scalar(localtime),": Done.\n" if $verbose;

}
