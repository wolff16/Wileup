#!/usr/bin/perl
use strict;
use warnings;
use List::MoreUtils qw(uniq);
use 5.010;
use Data::Dumper qw(Dumper);


# gehe zeilenweise durch die datei
# erzeuge hashes mit den cosmic ids als key -> values sind die verschiedenen columns die uns interessieren
# hinterher werden alle hashes in tabulated table format ausgeben

# from cosmic
# Substitutions involve the substitution of a single nucleotide and they are annotated 
# using syntax derived from HGVS nomenclature recommendations 
# [http://varnomen.hgvs.org/].

# In COSMIC v70 (August 2014) we have applied filtering to the dataset. We have excluded 
# data from any sample with over 15,000 mutations. In addition, we have flagged all 
# known SNPs as defined by the 1000 genomes project, dbSNP and a panel of 378 normal (non-cancer) 
# samples from Sanger CGP sequencing. Although all data are included in our download files, 
# we have excluded flagged mutations from the website.

# AA Mutation
# The change that has occurred in the peptide sequence as a result of the mutation. 
# Syntax is based on the recommendations made by the Human Genome Variation Society. 
# The mutation type is shown in brackets after the mutation string. 
# A description of each type can be found below in the section entitled Mutation Type.

# CDS Mutation
# The change that has occurred in the nucleotide sequence as a result of the mutation. 
# Syntax is identical to the method used for the peptide sequence.

# The mutation type is used to describe the type of mutation that has occurred.

# Mutation Types:


#     Nonsense :      A substitution mutation resulting in a termination codon, 
#                     foreshortening the translated peptide.

#     Missense :      A substitution mutation resulting in an alternate codon, 
#                     altering the amino acid at this position only.

#     Coding silent : A synonymous substitution mutation which encodes the same 
#                     amino acid as the wild type codon.

#     Intronic :      A substitution mutation outside the coding domains. No interpretation is 
#                     made as to its effect on splice sites or nearby regulatory regions.

#     Complex :       A compound mutation which may involve multiple insertions, deletions 
#                     and substitutions.

#     Unknown :       A mutation with no detailed information available.      

###############################################################
# add the location of your cosmic annotation file here        #
# i got it under Annotation/...								  #
###############################################################
my $cosmic_ann="Annotation/CosmicMutantExportCensus.tsv"	;

my %Gene_name = ();
my %Primary_site = ();
my %histology = ();
#$VAR18 = 'Mutation CDS';
#$VAR19 = 'Mutation AA';
my %Mutation_strand = ();
my %Mutation_Description = ();
my %Mutation_somatic_status= ();
my %Pubmed_PMID = ();
my %Mut_position = ();
my %Mut_CDS = ();
#my %Tumour_origin = ();
my @unique_cosmics = ();
my @cosmics = (); #store alle unique cosmic ids to iterate over them as keys


open my $fh, '<', $cosmic_ann or die $!;
  chomp(my @lines = <$fh>);
close($fh);

# grab the header line now to get order of fields via the hashes
my $b=0;
my $headerline = shift  @lines;
my @keys = split (/\t+/, $headerline);

foreach my $line (@lines) {
  my @splits = (split /\t/, $line );
  #print STDERR Dumper @splits;
  my $cosmic =  $splits[16]; # getting the cosmic id which serves as key for our hashes
  my $primvalue = $splits[8];
  my $mut_cds = $splits[17];
  my $Mut_Descr = $splits[19];
  my $Mut_pos   = $splits[23];
  my $Mut_strand = $splits[24];
  my $Mut_som = $splits[29];
  my $pubm =$splits[30];
  my $histvalue = $splits[11];
  my $hists1value = $splits[12];
  my $hists2value = $splits[13];
  my $hists3value = $splits[14];
  
  push(@cosmics,$cosmic);

  # if exist we overwrite it, theres allways the same gene name for one cosmic id
  $Gene_name{"$cosmic"} = $splits[0];
  $Mutation_Description{"$cosmic"} = $Mut_Descr;
  $Mut_position{"$cosmic"} = $Mut_pos;
  $Mut_CDS{"$cosmic"} =$mut_cds;
  $Mutation_somatic_status{"$cosmic"} = $Mut_som;
  $Mutation_strand{"$cosmic"} = $Mut_strand;
  $Pubmed_PMID{"$cosmic"} = $pubm;

  # we can have multiple primary site for one cosmic id, we want to append here
  # hashes can only store one skalar -> solution: store array reference containing multiple values
  # these need to be unique
  
  if(exists $Primary_site{"$cosmic"}){ 
    # we are in a line were there could be multiple prime site for the $cosmic id
    # check if our hash has a primary site allready associated with this cosmic id
    if ( grep( /$primvalue/, $Primary_site{"$cosmic"} )){
    
      # value already present nothing to do!
    
    }else{
      
      #if not add it to the string
      
      my $temp = $Primary_site{"$cosmic"};
      $Primary_site{"$cosmic"} = "$temp,$primvalue";
      
      # corner case: NS -> find an updated value
      # we got now tissues together with not specified
      # delete the NS from the string
      
      $Primary_site{"$cosmic"} =~ s/NS,//g;
      $Primary_site{"$cosmic"} =~ s/,NS//g;
    }
  }else{
    
    # we didnt associate this primary site with our cosmic id so we add it
        
    $Primary_site{"$cosmic"} = $primvalue;
  }

  # we check histology and its subtypes here 
  # we want to stack all possible different 
  # histological differentiations per cosmic id
  # and allways take the most specified definition if not "NS"
  
  if(exists $histology{"$cosmic"}){ 

    # check if our hash has the hists3 allready associated with this cosmic id

    if ( grep( /$hists3value/, $histology{"$cosmic"} ) && ($hists3value ne "NS") ){

      # value already present nothing to do!

    }elsif ($hists3value ne "NS") {

      # we got a hist 3 classification
      # but specified the string before therefore we append

      my $temp = $histology{"$cosmic"};
      $histology{"$cosmic"} ="$temp,$hists3value";
    }elsif ( grep( /$hists2value/, $histology{"$cosmic"} ) && ($hists2value ne "NS")){

        #somehow allready added   

    }elsif ($hists2value ne "NS") { 

      # we got a hist 2 classification
      # but specified the string before therefore we append

      my $temp = $histology{"$cosmic"};
      $histology{"$cosmic"} ="$temp,$hists2value";

    }elsif ( grep( /$hists1value/, $histology{"$cosmic"} ) && ($hists1value ne "NS")){

        #allready added   

    }elsif ($hists1value ne "NS") { 

      # we got a hist 1 classification
      # but specified the string before therefore we append

      my $temp = $histology{"$cosmic"};
      $histology{"$cosmic"} ="$temp,$hists1value";
    }elsif ( grep( /$histvalue/, $histology{"$cosmic"} ) && ($histvalue ne "NS") ){
 
        # allready added   
 
    }elsif ($histvalue ne "NS") { 
 
      # we got a hist classification
      # but specified the string before therefore we append
 
      my $temp = $histology{"$cosmic"};
      $histology{"$cosmic"} ="$temp,$histvalue";
 
    }else{ 
 
      # all string hist checked and only "NS" left
 
      my $temp = $histology{"$cosmic"};
      $histology{"$cosmic"} ="$temp,NS";
 
     }
 
    # corner case: NS -> find an updated value
    # we got now tissues together with not specified (NS)
    # delete the NS from the string
 
    $histology{"$cosmic"} =~ s/NS,//g;
    $histology{"$cosmic"} =~ s/,NS//g;
    
  }else{
 
    # no value exists so we set the value for the first time
    # check if our hash has the hists3 allready associated with this cosmic id
 
    if ($hists3value ne "NS") {
 
      # we got a hist 3 classification
      # but specified the string before therefore we append
 
      $histology{"$cosmic"} = "$hists3value";
 
    }elsif ($hists2value ne "NS") { 
 
      # we got a hist 2 classification
      # but specified the string before therefore we append
 
      $histology{"$cosmic"} = "$hists2value";
 
    }elsif ($hists1value ne "NS") { 
 
      # we got a hist 1 classification
      # but specified the string before therefore we append
 
      $histology{"$cosmic"} = "$hists1value";
 
    }elsif ($histvalue ne "NS") { 
 
      # we got a hist classification
      # but specified the string before therefore we append
 
      $histology{"$cosmic"} = "$histvalue";
 
    }else{ 
 
      # all string hist checked and only "NS" left
    
      $histology{"$cosmic"} ="NS";
 
    }
  }
  $b++;
  print STDERR"Processed $b lines\n" if $b % 10000 == 0;
 
}  

# sort cosmics as unique identifiers for later writeout

@unique_cosmics = uniq @cosmics;
@unique_cosmics= sort @unique_cosmics;

print "CosmicID\tGene_Name\tMutation_genome_position\tMutation_strand\tMutation_CDS\tPrimary_site\tHistology\tMutation_Description\tMutation_somatic_status\tPubmed_PMID\n";
foreach my $uc (@unique_cosmics)
{ #handling point mutations for now with the form of: c.1022G>T etc
  # and the position is given, there are blank ones in cosmic for some reason
  if (($Mut_CDS{$uc}  =~ m/[c]\.\d+([ACGTacgt])>([ACGTacgt])/)and($Mut_position{$uc} =~ m/\d+\:\d+[-]\d+/)){
    print "$uc\t$Gene_name{$uc}\t$Mut_position{$uc}\t$Mutation_strand{$uc}\t$Mut_CDS{$uc}\t$Primary_site{$uc}\t$histology{$uc}\t";
    print "$Mutation_Description{$uc}\t$Mutation_somatic_status{$uc}\t$Pubmed_PMID{$uc}\n";
  }
}
#debug prints:
#print STDERR Dumper @keys;
#print "Key: $_ and Value: $Primary_site{$_}\n" foreach (keys%Primary_site);
#print STDERR "Key: $_ and Value: $histology{$_}\n" foreach (keys%histology);