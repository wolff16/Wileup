# Wileup.pl 
## A tool for variant detection & annotation utilizing RNA-Seq data

For calling RNA-Seq specific mutations we implemented a variation of pileuping
nucleotide bases at each position in the transcriptome, utilizing mpileup from
samtools. 

In case all possible mutation in the RNA-Seq data should be checked,
a mpileup of all base positions in the RNA-Seq data is performed. Afterwards 
the distribution of bases in each position is saved and evaluated to annotate
them at positions were supplemental database information
(CIVIC, ClinVAR, Cosmic), parsed accordingly in the script, is available. 

In case the -panelmode flag is selected a list of 442 nucleotide positions 
is selected and only for these positions the mpileup is called. A minimum of 
3 reads or 10% of the reads supporting the alternative variant are used as 
default thresholds. The output comprises the distribution of bases at each
position, the decision whether the position contains an SNV, and the clinical
annotations from CIViC and CGI. 


If you use Wileup.pl in your work please cite *Using RNA-Seq data for the detection of a panel of clinically relevant mutations (A Wolff, J Perera-Bel, et al., Stud Health Technol Inform)*
**update with final cite**
## QuickStart

Prerequisite for using wileup.pl are [samtools](https://github.com/samtools/samtools/releases/) version 1.5 or higher and [perl](https://www.perl.org/get.html) v5.22.1 or higher.
Downlaod the human genome: [Homo_sapiens.GRCh38.dna.primary_assembly.fa](ftp://ftp.ensembl.org/pub/release-91/fasta/homo_sapiens/).
For easier use put it in the same directory as in the example commands or adapt the path accordingly.

All used annotation file are allready present in the Annotation folder and only need to be recreated if newer versions are necessary.

### Example using Wileup in --panelmode
```
perl wileup.pl -r Annotation/Homo_sapiens.GRCh38.dna.primary_assembly.fa -a testdata/one_promille_of_bam_file.bam -v -m -p Annotation/CGI_CIVIC_SNVs_20171010.csv 
```

-r reference fasta file
-a alignment file
-v verbose mode on
-m used to flag --panelmode
-p original CIVIC .csv file
### Example using Wileup on a complete run

```
perl wileup.pl -r Annotation/Homo_sapiens.GRCh38.dna.primary_assembly.fa -a testdata/one_promille_of_bam_file.bam -c Annotation/variant_summary.txt -o Annotation/cosmicMutantExportCensus20171102_parsed.tsv -i Annotation/CGI_CIVIC_SNVs_20171010.csv -v -s
```

-r reference fasta file
-a alignment file
-c database file from clinvar
-o parsed cosmic file from cosmic2table.pl
-i original CIVIC .csv file
-v verbose mode on
-s skipps variants, were we don't have any annotation support
-p original CIVIC .csv file




## Updating Annotation files

The Genome used for the annotation is: [Homo_sapiens.GRCh38.dna.primary_assembly.fa](ftp://ftp.ensembl.org/pub/release-91/fasta/homo_sapiens/dna/Homo_sapiens.GRCh38.dna.primary_assembly.fa.gz.).

Here are the 3 databases version we used for the *complete* and *panelmode* analysis part of Wileup:


**civic** version from Nov. 2017:
```
wget https://civic.genome.wustl.edu/downloads/01-Nov-2017/01-Nov-2017-VariantSummaries.tsv
```

**clinvar** from August 2017:
```
wget ftp://ftp.ncbi.nlm.nih.gov/pub/clinvar/tab_delimited/variant_summary.txt.gz.
```

**Cosmic** from Sept. 2017 :
register and download  *CosmicMutantExportCensus.tsv* from https://cancer.sanger.ac.uk/cosmic .

### Creating the CGI/CIVIC database

This database is used for the -panelmode part of the perl script.

In the folder *Annotation* you will find the 442 mutations with clinical and drug response annotation selected from the complete CGI/CIVIC database file
 *Annotation/CGI_CIVIC_SNVs_20171010.csv* together with its matched list file *Annotation/20180521_panel.list* for the samtools mpileup. The panel.list only
 consist of the columns 'chr' and 'pos' seperated by tabs for the 442 mutations as a basic requirement for pileuping. 

1.) 
**CIVIC**: Downloaded from https://civic.genome.wustl.edu/#/releases and download the latest .tsv from the tab Evidence Summaries.
2.)
**CGI**: Downloaded from https://www.cancergenomeinterpreter.org/biomarkers
3.)
Have a look into the R.script under **Annotation/actionable_SNVs.R** update the links with your new CIVIC and CGI files and rerun the script within
 the programming language R.

### Creating the COSMIC database

1.) update the input file name in cosmic2table.pl or rename your newest cosmic version to CosmicMutantCencus.tsv and backup the old one.  
There is a default file under **Annotation/CosmicMutantCencus.tsv**. The parser will look for this file and print the parsed result to STDOUT.
   
```
perl cosmic2table.pl > cosmic_parsed.tsv
```
### Creating the ClinVAR database

It can be used as is.

