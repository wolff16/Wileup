Cancer Biomarkers database
--------------------------

The Cancer Biomarkers database is provided via two files. The first 
(cgi_biomarkers.tsv) is a simplified version in which variants are grouped 
in a single row when leading to the same drug effect in a given cancer 
type(s). In the second file (cgi_biomarkers_per_variant.tsv), each variant 
is stated in a separate row and --in case of mutations-- genomic coordinates
are included. For the latter, note that the nucleic acid - amino acid 
correspondance is retrieved by using the longest transcript (except for a 
subset of 109 genes in which the annotation transcript was manually selected). 



cgi_biomarkers.tsv
------------------

Biomarker
Gene
Alteration type
Alteration
Targeting
Drug status
Drug family
Drug
Association
Evidence level
Assay type
Source
Curator
Curation date
Primary Tumor type
Primary Tumor acronym
Metastatic Tumor Type
TCGI included
Comments
Drug full name


cgi_biomarkers_per_variant.tsv
------------------------------

Alteration
Alteration type
Assay type
Association
Biomarker
Curator
Drug
Drug family
Drug full name
Drug status
Evidence level
Gene
Metastatic Tumor Type
Primary Tumor type
Primary Tumor acronym
Source
Targeting
individual_mutation
transcript
gene
strand
region
info
cDNA
gDNA

